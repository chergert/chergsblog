<html>
 <head>
  <title>${blog.title}</title>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>
  <link rel="stylesheet" type="text/css" href="css/global.css"/>
 </head>
 <body>
  <div id="blog">
   <div id="blog-title"><a href="index.html">${blog.title}</a></div>
   <div class="blog-toc">
    <ul class="blog-toc-years">
% for year in blog.iter_years():
     <li>
      <div class="blog-toc-year">${year}</div>
      <ul class="blog-toc-months">
% for month in blog.iter_months(year):
       <li>
        <div class="blog-toc-month">${month_name(month)}</div>
        <ul class="blog-toc-entries">
% for entry in blog.by_month[(year, month)]:
        <li>
         <div class="blog-toc-entry-title"><a href="${entry.permalink()}">${entry.title|h}</a></div>
         <div class="blog-toc-entry-date">${format_date(entry.created_at)|h}</div>
        </li>
% endfor
        </ul>
       </li>
% endfor
      </ul>
     </li>
% endfor
    </ul>
   </div>
  </div>
 </body>
</html>
