<html>
 <head>
  <title>${blog.title}</title>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>
  <link rel="stylesheet" type="text/css" href="../../css/global.css"/>
 </head>
 <body>
  <div id="blog">
   <div id="blog-title"><a href="../../index.html">${blog.title}</a></div>
   <div class="blog-entries">
% for entry in entries:
    <a name="${entry.aname}"></a>
    <div class="blog-entry">
     <div class="blog-entry-title"><a href="#${entry.aname}">${entry.title}</a></div>
     <div class="blog-entry-attributes">
      <div class="blog-entry-date">${format_date(entry.created_at)}</div>
% if entry.author_email:
      <div class="blog-entry-author"><a href="mailto:${entry.author_email}">${entry.author}</a></div>
% else:
      <div class="blog-entry-author">${entry.author}</div>
% endif
      </div>
     <div class="blog-entry-body">${entry.body.to_html()}</div>
    </div>
% endfor
   </div>
   <div id="footer">
    <ul>
     <li><a href="../../toc.html">More Entries</a></li>
    </ul>
   </div>
  </div>
 </body>
</html>
