<?xml version="1.0" encoding="UTF-8"?>
<rss    version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
      xmlns:wfw="http://wellformedweb.org/CommentAPI/"
       xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
       xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
 <channel>
  <title>${blog.title|x}</title>
  <atom:link href="${blog.url_base}/rss2.xml"
              rel="self"
             type="application/rss+xml"/>
  <link>${blog.url_base}</link>
  <description>${blog.description|x}</description>
  <lastBuildDate>${rfc2822(blog.get_last_modified_at())}</lastBuildDate>
  <generator>file:///dev/null</generator>
  <language>en</language>
  <sy:updatePeriod>hourly</sy:updatePeriod>
  <sy:updateFrequency>1</sy:updateFrequency>
% for entry in blog.entries[:10]:
  <item>
   <title>${entry.title|x}</title>
   <link>${blog.url_base}/${entry.created_at.year}/${entry.created_at.month}/${entry.created_at.day}.html</link>
   <pubDate>${rfc2822(entry.created_at)}</pubDate>
   <dc:creator>${entry.author|x}</dc:creator>
   <guid isPermalink="false">${blog.url_base}/${entry.created_at.year}/${entry.created_at.month}/${entry.created_at.day}.html</guid>
   <content:encoded><![CDATA[${entry.body.to_html()}]]></content:encoded>
  </item>
% endfor
 </channel>
</rss>
