#!/usr/bin/env python

#
# blog.py
#
# Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
#
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
This module contains a minimalistic blogging platform that is, and likely
always will be, a work-in-progress. The goal is a very simple blogging
format that consists of text files with a simple header followed by text.
This script will take care of the rest. Some basic theming will be possible
and contained within the theme directory. All content will be rendered to
HTML and CSS with perhaps a sprinkling of JavaScript. I fully intend for
this to be an offline platform that allows for hosting on a site that only
serves static content.

My primary use-case is this.

  1) cd into my blog directory.
  2) open a new file with a header such as:
     Author: Christian Hergert
     Date: $(date -R)
     Title: Blogging for future humanity.
  3) Start writing the text of the blog post.
  4) git commit
     At this point, git should have generated the HTML, CSS and JavaScript
     for the static content of my site.
  5) I'm not sure how I want to push it to my server yet.
"""

USAGE = \
"""
blog.py - A simply blogging platform for Christian Hergert

This is a blogging platform for myself. If it doesn't do what you need, that
is probably because it wasn't written for you. However, if you do happen to
like it and add something to it, feel free to share that back with me.

Usage:
    blog.py [OPTIONS] [FILENAME | DIRECTORY]

Options:
    -o, --output=       The directory to output generated content.
                        The default is "generated".
    -t, --theme=        The theme to use for generation.
                        The default is "default".
    -h, --help          Show application usage.
"""

def fromRfc2822(text):
    """
    Parses a 2822 formatted date to a UTC based datetime.
    """
    from datetime import datetime, timedelta
    from email.utils import parsedate_tz
    import time
    parts = parsedate_tz(text)
    dt = datetime(*parts[:6])
    # ugly hack to get things in local time.
    timestamp = time.mktime(dt.timetuple())
    dt = datetime.fromtimestamp(timestamp)
    return dt

def toIso8601(dt):
    return dt.isoformat()

def toRfc2822(dt):
    import time
    import email.utils
    return email.utils.formatdate(time.mktime(dt.utctimetuple()))

def month_name(month):
    from datetime import datetime
    d = datetime(2011, month, 1)
    return d.strftime('%B')

def format_date(dt, format=''):
    if not format:
        return dt.strftime('%A, %B %d, %Y')
    else:
        return dt.strftime(format)

def parseAuthor(text):
    from email.utils import parseaddr
    text = text.strip()
    if '@' in text:
        try:
            return parseaddr(text)
        except:
            pass
    return (text, '')

class Item(object):
    def chain(self, item):
        return False

    @classmethod
    def test(cls, line):
        return False

    def to_html(self):
        return ''

class Link(Item):
    url = None
    text = None

    def __init__(self, text):
        parts = text[2:-3].split(' ', 1)
        if len(parts) == 2:
            self.url, self.text = parts
        else:
            self.url, self.text = parts[0], parts[0]

    @classmethod
    def test(cls, text):
        text = text.strip()
        return text.startswith('[[') and text.endswith(']]')

    def to_html(self):
        return '<a href="%s">%s</a>' % (self.url, self.text)

class Title(Item):
    level = None
    text = None

    def __init__(self, text):
        self.level = 0
        text = text.strip()
        for i,c in zip(xrange(0, len(text)), text):
            if c == '=':
                self.level += 1
            else:
                self.text = text[i:-(i+1)].strip()
                break

    @classmethod
    def test(cls, line):
        line = line.strip()
        return line.startswith('=') and line.endswith('=')

    def to_html(self):
        return '<h%d>%s</h%d>' % (self.level, self.text, self.level)

class Paragraph(Item):
    text = None
    _finished = False

    def __init__(self, text=''):
        if text.strip():
            self.text = [text]
        else:
            self.text = []

    def chain(self, item):
        if not self._finished:
            if isinstance(item, Paragraph):
                self.text += item.text
                self._finished = not item.text
                return True
            elif isinstance(item, Link):
                self.text.append(item)
                self._finished = not item.text
                return True
        return False

    @classmethod
    def test(cls, line):
        return True

    def to_html(self):
        s = []
        for i in self.text:
            if isinstance(i, Item):
                s.append(i.to_html())
            else:
                s.append(i.replace('\n', ' '))
        s = ''.join(s).strip()
        if s:
            return '<div class="blog-para">%s</div>' % s
        return ''

    def __repr__(self):
        return '<Paragraph(%s)>' % repr(self.text)

class OrderedList(Item):
    items = None

    def __init__(self, text=''):
        self.items = [Paragraph(text.split('*', 1)[1].strip())]

    def __repr__(self):
        return '<OrderedList(%s)>' % repr(self.items)

    def chain(self, item):
        if isinstance(item, OrderedList):
            self.items.extend(item.items)
            return True
        return False

    @classmethod
    def test(cls, line):
        return line.strip().startswith('*')

    def to_html(self):
        html = []
        html.append('<ul>')
        for item in self.items:
            li = '<li>%s</li>' % item.to_html()
            html.append(li)
        html.append('</ul>')
        return ''.join(html)

class NumberedList(Item):
    items = None

    def __init__(self, text=''):
        self.items = [Paragraph(text.split('#', 1)[1].strip())]

    def __repr__(self):
        return '<NumberedList(%s)>' % repr(self.items)

    def chain(self, item):
        if isinstance(item, NumberedList):
            self.items.extend(item.items)
            return True
        return False

    @classmethod
    def test(cls, line):
        return line.strip().startswith('#')

    def to_html(self):
        html = []
        html.append('<ol>')
        for item in self.items:
            li = '<li>%s</li>' % item.to_html()
            html.append(li)
        html.append('</ol>')
        return ''.join(html)

class CodeTitle(Item):
    text = None

    def __init__(self, text):
        self.text = text[3:].strip()

    @classmethod
    def test(cls, line):
        return line.strip().startswith('>>>')

    def to_html(self):
        return '<div class="blog-entry-code-header">%s</div>' % self.text

class Code(Item):
    """
    This item represents a block of source code. It is contained
    within an area of '''. You can specify options on the line such as:

        ''' lang=c
        #include <stdio.h>
        '''
    """
    _finished = False
    lang = None
    lineno= True
    text = None

    def __init__(self, line):
        self.text = ''
        line = line.replace("'''", '').strip()
        if line:
            parts = line.split('=')
            newparts = []
            for p in parts:
                newparts.extend(p.split(' '))
            parts = newparts
            if len(parts) % 2 == 1:
                print 'Invalid code line:', line
                sys.exit(-1)
            for i in range(0, len(parts), 2):
                key = parts[i]
                if key == 'lang':
                    self.lang = parts[i+1]
                elif key == 'lineno':
                    self.lineno = parts[i+1] != '0'

    def chain(self, item):
        if self._finished:
            return False
        elif isinstance(item, Code):
            self._finished = True
            return True
        return False

    @classmethod
    def test(cls, line):
        return line.startswith("'''");

    def to_html(self):
        from pygments import highlight
        from pygments.lexers import get_lexer_by_name
        from pygments.formatters import HtmlFormatter

        if not self.lang:
            # TODO: Really sanitize.
            htmlize = lambda h: h
            return '<div class="blog-entry-code"><pre>%s</pre></div>' % htmlize(self.text)

        if self.lineno:
            h = HtmlFormatter(linenos='table')
        else:
            h = HtmlFormatter()
        l = get_lexer_by_name(self.lang)
        html = highlight(self.text.rstrip(), l, h)
        idx = html.rfind('\n')
        html = html[:idx] + html[idx+1:]
        return '<div class="blog-entry-code">%s</div>' % html

class Body(object):
    """
    This object helps us build a body of an entry by chaining
    items to the previous item in the list if possible.
    """
    items = None

    def __init__(self):
        self.items = []

    def __repr__(self):
        return '<Body(%s)>' % repr(self.items)

    def append(self, item):
        if self.items and self.items[-1].chain(item):
            return
        self.items.append(item)

    def to_html(self):
        return ''.join((i.to_html() for i in self.items))

class Entry(object):
    """
    This object contains an entry within the blog.
    """
    aname = None
    author = None
    author_email = None
    created_at = None
    modified_at = None
    title = None
    body = None

    def __init__(self):
        self.body = Body()

    def __repr__(self):
        return '<Entry(%s by %s)>' % (repr(self.title), self.author)

    def load_from_file(self, filename):
        """
        Parses filename to build an entry instance.
        """
        import codecs
        f = codecs.open(filename, 'r', 'utf-8')
        self.load(f)

    def load(self, sequence):
        """
        Loads an entry from the contents found in sequence. Sequence
        should be a line based iterator.
        """
        inBody = False
        sequence = iter(sequence)

        while not inBody:
            for line in sequence:
                line = line.strip()
                if not line:
                    inBody = True
                    break
                header, value = [a.strip() for a in line.split(':', 1)]
                header = header.lower()
                if header == 'author':
                    self.author, self.author_email = parseAuthor(value)
                elif header == 'date':
                    self.created_at = fromRfc2822(value)
                    if not self.modified_at:
                        # TODO: Get from stat() atime.
                        self.modified_at = self.created_at
                elif header == 'title':
                    from urllib import quote
                    self.title = value
                    aname = value.lower().replace(' ', '-').strip()[:64]
                    self.aname = quote(aname)

        for line in sequence:
            if OrderedList.test(line):
                self.body.append(OrderedList(line))
            elif NumberedList.test(line):
                self.body.append(NumberedList(line))
            elif CodeTitle.test(line):
                self.body.append(CodeTitle(line))
            elif Code.test(line):
                self.body.append(Code(line))
                # only paragraphs until we get to code
                for line in sequence:
                    if not Code.test(line):
                        self.body.items[-1].text += line
                    else:
                        self.body.append(Code(line))
                        break
            elif Link.test(line):
                self.body.append(Link(line))
            elif Title.test(line):
                self.body.append(Title(line))
            else:
                self.body.append(Paragraph(line))

    def permalink(self):
        """
        Generates a permalink URL to the particular entry.
        """
        args = [str(v).zfill(2) for v in self.created_at.timetuple()[:3]]
        args += [self.aname]
        return '%s/%s/%s.html#%s' % tuple(args)

class Blog(object):
    """
    This object contains everything that is in a blog.
    """
    title = 'Engineering, or something.' # TODO: make configurable
    description = ''
    url_base = 'https://audidude.com'

    def __init__(self):
        self.by_year = {}
        self.by_month = {}
        self.by_day = {}
        self.entries = []

    def add_from_file(self, filename):
        """
        This method will parse a blog file and add it's content
        to the blog instance.
        """
        entry = Entry()
        entry.load_from_file(filename)
        self.add_entry(entry)

    def add_entry(self, entry):
        """
        This method will add an entry to the blog instance.
        """
        by_year = (entry.created_at.year,)
        by_month = (entry.created_at.year,
                    entry.created_at.month)
        by_day = (entry.created_at.year,
                  entry.created_at.month,
                  entry.created_at.day)

        if by_year not in self.by_year:
            self.by_year[by_year] = []
        self.by_year[by_year].append(entry)
        self.by_year[by_year].sort(lambda a,b: cmp(b.created_at, a.created_at))

        if by_month not in self.by_month:
            self.by_month[by_month] = []
        self.by_month[by_month].append(entry)
        self.by_month[by_month].sort(lambda a,b: cmp(b.created_at, a.created_at))

        if by_day not in self.by_day:
            self.by_day[by_day] = []
        self.by_day[by_day].append(entry)
        self.by_day[by_day].sort(lambda a,b: cmp(b.created_at, a.created_at))

        self.entries.append(entry)
        self.entries.sort(lambda a,b: cmp(b.created_at, a.created_at))

    def generate(self, directory='.', theme='default'):
        """
        This method will generate the static HTML, CSS, and JavaScript for the
        given blog entries.
        """
        from mako.lookup import TemplateLookup
        from mako.template import Template
        import os

        templateDir = os.path.join('themes', theme)
        templateLookup = TemplateLookup(directories=[templateDir],
                                        output_encoding='utf-8',
                                        encoding_errors='replace')

        os.system('cp -a "%s" "%s"' % (os.path.join(templateDir, 'css'), directory))

        self.generate_page(templateLookup, directory, 'index.html')
        self.generate_page(templateLookup, directory, 'toc.html')
        self.generate_page(templateLookup, directory, 'rss2.xml')

        for (year, month, day), entries in self.by_day.iteritems():
            filename = '%s.html' % str(day).zfill(2)
            thisdir = os.path.join(directory,
                                   str(year).zfill(2),
                                   str(month).zfill(2))
            if not os.path.isdir(thisdir):
                os.makedirs(thisdir)
            self.generate_page(templateLookup, thisdir, filename,
                               'day.html.mako', entries=entries)

    def generate_page(self, lookup, directory, dest, src=None, **kwargs):
        import os
        import lxml.etree

        helpers = {
            'rfc2822': toRfc2822,
            'iso8601': toIso8601,
            'month_name': month_name,
            'format_date': format_date,
        }

        kwargs['blog'] = self
        kwargs.update(helpers)
        src = src or (dest + '.mako')
        target = os.path.join(directory, dest)
        template = lookup.get_template(src)
        data = template.render(**kwargs)

        parser = None
        if dest.endswith('.html'):
            parser = lxml.etree.HTMLParser
        elif dest.endswith('.xml'):
            parser = lxml.etree.XMLParser

        if parser:
            try:
                parsed = lxml.etree.tostring(lxml.etree.fromstring(data,
                    parser=parser(remove_blank_text=True, recover=False)),
                    xml_declaration=True,
                    encoding='utf-8')
                data = parsed
            except Exception, e:
                print 'Failed to parse', target
                print repr(e)
                print data

        with file(target, 'w') as stream:
            stream.write(data)

    def get_last_modified_at(self):
        """
        Fetches the time of the most recent edit to an entry.
        """
        from datetime import datetime
        most_recent = datetime(1970, 1, 1)
        for entry in self.entries:
            entry_touched_at = max(entry.created_at, entry.modified_at)
            if entry_touched_at > most_recent:
                most_recent = entry_touched_at
        return most_recent

    def iter_years(self):
        years = [y for y, in self.by_year]
        years.sort()
        return iter(reversed(years))

    def iter_months(self, year):
        months = []
        for (y, m) in self.by_month.iterkeys():
            if y == year:
                months.append(m)
        months.sort()
        return iter(reversed(months))

if __name__ == '__main__':
    from getopt import getopt
    import os
    import sys

    shortOpts = 'ho:t:'
    longOpts = ['help', 'output=', 'theme=']

    try:
        opts, args = getopt(sys.argv[1:], shortOpts, longOpts)
    except Exception, e:
        print >> sys.stderr, 'Could not parse command arguments.'
        print >> sys.stderr, USAGE
        sys.exit(-1)

    output = None
    theme = None

    for o,a in opts:
        if o in ('-h', '--help'):
            print >> sys.stdout, USAGE
            sys.exit()
        elif o in ('-o', '--output'):
            output = a
        elif o in ('-t', '--theme'):
            theme = a

    if not output:
        output = 'generated'

    if not theme:
        theme = 'default'

    if not args:
        args = ['entries']

    blog = Blog()

    def parse(f):
        blog.add_from_file(f)

    for path in args:
        if os.path.isdir(path):
            for f in os.walk(path):
                files = f[-1]
                d = os.path.join(*f[:-2])
                for path in (os.path.join(d, f) for f in files):
                    if path.endswith('~'):
                        continue
                    if path.endswith('.swp'):
                        continue
                    if os.path.isfile(path):
                        parse(path)
        elif os.path.isfile(path):
            parse(path)

    if not os.path.isdir(output):
        os.mkdir(output)

    blog.generate(directory=output, theme=theme)
