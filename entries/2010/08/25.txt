Author: Christian Hergert <chris@dronelabs.com>
Date: Thu, 26 Aug 2010 01:09:18 -0000
Title: GDateTime

Some readers may remember that I spent some time working on a DateTime structure for GLib last year.  After much refinement by others better than myself, it has been included in GLib master.  Much thanks to Thiago Sousa Santos, Emmanuele Bassi, and the countless reviewers.

You can find some quick examples <a href="http://github.com/chergert/glib-cookbook/blob/master/datetime.c">here</a>.

GDateTime is both immutable and reference counted.  See <a href="http://git.gnome.org/browse/glib/tree/glib/gdatetime.h">gdatetime.h</a> for relevant API.

''' lang=c
{
  GDateTime *dt;
  char *str;

  dt = g_date_time_new_now();
  str = g_date_time_format(dt, "It is currently %x %X %z");
  g_print("%s\n", str);

  g_date_time_unref(dt);
  g_free(str);
}
'''
