Author: Christian Hergert <chris@dronelabs.com>
Date: Wed, 27 Oct 2010 17:04:54 -0000
Title: Gibson Les Paul - 58 Custom

<p>Short post today. I purchased a new Gibson Les Paul from the Custom Shop last week. It's a 58 reissue. The sound is incredible.</p>

<p>I'll be in Boston next week for <a href="http://www.linuxplumbersconf.org/2010/">Linux Plumbers Conference</a> and the <a href="http://live.gnome.org/Boston2010">Boston Gnome Summit</a>. I've been working on a new project that I think developers are going to love. It combines a lot of the things I've blogged about over the last year. Catch me while I'm there for a demo.

<img alt="" src="http://x.dronelabs.com/chris/blog/pictures/IMG_1345.JPG" />

<img alt="" src="http://x.dronelabs.com/chris/blog/pictures/IMG_1346.JPG" />
