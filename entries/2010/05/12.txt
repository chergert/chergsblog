Author: Christian Hergert <chris@dronelabs.com>
Date: Thu, 13 May 2010 04:05:36 -0000
Title: Conquering DBus

<p>I haven't written in a while because I've been pretty heads down on a project I'm working on.  Hopefully, it will be in a shape where it makes sense to share with everyone before too long.</p>

<p>In this project, I needed an RPC system that could have multiple transport and be fully asynchronous (with <a href="http://library.gnome.org/devel/gio/stable/GAsyncResult.html">GAsyncResult</a>, etc).  So I took a little time and wrote a crappy little python program that generates the following peices in GObject/C from a simple input format.</p>

<ul>
<li>Transport agnostic (currently has DBus backend).
<li>A client library with lowlevel RPC calls and an object model built on top of the lowlevel calls.  Consumer can choose which best fits their use case. (Object model not finished).</li>
<li>A server implementation (also transport agnostic).</li>
<li>Fully Asynchronous.  Synchronous implementation is built on top of the asynchronous implementation.</li>
<li>Unit tests (for testing RPC round trips, etc).</li>
<li>Generated code should be clean to read and debug.</li>
<li>Tracing support for debugging. (Use -DDISABLE_TRACING to turn off).</li>
<li>Generate DBus Introspection XML.</li>
</ul>

<p>Anyway, code is <a href="http://github.com/chergert/yosemite">here</a>[1].  There is a sample video below where I generate a mockup service for getting information on "Books" and "Authors".  See the Makefile in the outdir directory for how to build things.  Keep in mind I have absolutely <b>NO INTENTION</b> of maintaining this.  But at minimum, I figured it might serve useful for someone.</p>

<!-- video controls="controls" src="http://x.dronelabs.com/chris/blog/screencasts/rpcgen1.ogv"><a href="http://x.dronelabs.com/chris/blog/screencasts/rpcgen1.ogv">Watch the video here</a>.</video -->

<p>So a quick example of the imput format would be like the following.  I've removed the header lines for succinctness.</p>

''' lang=python
object Book {
    # books are identified by a unique int (string also supported).
    id int

    """
    Document your rpc's with python style docs.  Comments persist into
    the generated code's gtkdoc.
    """
    rpc get_name (out string name)

    """
    Retrieves the book's author.
    """
    rpc get_author (out Author author)
}

object Author {
    id int

    """
    Retrieves the books by this author.
    """
    rpc get_books (out Book[] books)
}

object Manager {
    # only one of these per application (/org/blah/Project/Manager).
    # might consider this later to simply be the default servce such as
    # (/org/blah/Project).
    singleton

    rpc get_books (out Book[] books)
    rpc get_authors (out Author[] authors)
}
'''

<p>Thats pretty much it.  The code is uglier than anything you've ever seen, but at least the generated C is pretty.  The code parser/generator is not super resilient or anything.  I pretty much hack it to have what I need as I go.</p>

[1] <a href="http://github.com/chergert/yosemite">http://github.com/chergert/yosemite</a>
