Author: Christian Hergert <chris@dronelabs.com>
Date: Tue, 23 Nov 2010 07:34:13 -0000
Title: Still hacking along on perfkit

Perfkit development has been continuing steadily. It's still just a toy of course; but I've gotten enough interest from people where this is a project I'd like to continue hacking on. I just added a gdk event source today that helps track down when your program is doing tons of updates. Once I get the data selection in place, you'll be able to dive in and explore the events. Maybe if I get some time during Thanksgiving vacation I can add that.

<img alt="Times of various gdk events" src="http://x.dronelabs.com/chris/blog/screenshots/perfkit/gdkevents.png" />
