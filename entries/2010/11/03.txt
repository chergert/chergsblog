Author: Christian Hergert <chris@dronelabs.com>
Date: Wed, 03 Nov 2010 21:58:47 -0000
Title: Perfkit

I've been busy working on something.  It is still just a toy.  It's called perfkit. <a href="http://github.com/chergert/perfkit">Fork me on Github</a>.

I'm getting older, and I'm having trouble remembering how to use every little tool under the sun.  From valgrind, to interesting proc files, to ftrace and perf.  You get the idea.  So I've been building an application that I can write plugins for to do this and hide the gory details behind a pretty interface.

If nobody likes it, thats fine; I'll probably keep hacking on this even for my own use.  However, after listening to <a href="http://blogs.gnome.org/mccann/">William Jon McCann</a> speak on Gnome OS today at <a href="http://www.linuxplumbersconf.org/2010/">Linux Plumbers Conference</a>, I think it is important to have a tool like this as part of the developer experience for Gnome 3.4 (or whatever release ends up targeting the developer experience).

I have some larger plans with this for more than just profiling, but I'll write about that at a later time when its more formulated in my mind.



<a href="http://www.youtube.com/watch?v=Y_adf-Jk6XA">http://www.youtube.com/watch?v=Y_adf-Jk6XA</a> Sorry if you don't like youtube, I apologize.  Wasn't really looking to spend all my available bandwidth on this.
