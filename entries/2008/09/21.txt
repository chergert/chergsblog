Author: Christian Hergert <chris@dronelabs.com>
Date: Sun, 21 Sep 2008 17:48:03 -0000
Title: python decorator and gtk mainloop

I just put together a neat little decorator to force a method to be called via the default gobject main loop.  It does this by wrapping a method call via an immediate timeout of the main loop.

''' lang=python
def callFromMain(f):
    import gobject
    def dispatch(items):
        args, kwargs = items
        return f(*args, **kwargs)
    def wrapper(*args, **kwargs):
        gobject.timeout_add(0, dispatch, (args, kwargs))
    return wrapper
'''

This works well for callbacks that come from other threads.

''' lang=python
@callFromMain
def resultCallback(*args):
    print args
'''
