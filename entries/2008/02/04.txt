Author: Christian Hergert <chris@dronelabs.com>
Date: Mon, 04 Feb 2008 08:01:46 -0000
Title: Rotate and TwinView for Dual Head

I remember long ago nvidia's proprietary driver did not support rotating monitors individually while under TwinView. This may still be the case, but this time I was interested in having both monitors rotated. Having the extra vertical space for Vim is much appreciated and websites are much more readable as an added benefit.

Anyway, to do this, we use a clever X hack.

>>> /etc/X11/xorg.conf
'''
Section "Device"
        Identifier      "nVidia Corporation NV43 [GeForce 6600 LE]"
        Driver          "nvidia"
        BusID           "PCI:1:0:0"
        Option          "MetaModes"           "1280x1024@60,1280x1024@60;"
        Option          "TwinView"            "True"
        Option          "TwinViewOrientation" "above"
        Option          "Rotate"              "left"
        Option          "AddARGBVisuals"      "True"
        Option          "AddARGBGLXVisuals"   "True"
EndSection
'''

You'll notice we set one monitor to be above another, and then rotate the entire group to the left.

And the important thing, compiz still runs speedy :-)
