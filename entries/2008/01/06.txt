Author: Christian Hergert <chris@dronelabs.com>
Date: Sun, 06 Jan 2008 17:02:06 -0000
Title: Database for GObjects

Not to long ago I found myself in need of a small object database for GObject to embed in projects.  Apparently, there are no projects providing this... Until now.

While inspired by <a href="http://log.emmanuelebassi.net/">Emmanuele Bassi's</a> <a href="http://folks.o-hand.com/~ebassi/git/json-glib.git">json-glib</a> library I went on to start hacking something quick together. The embedded DB is called <a href="http://pantlesssoftware.com/projects/catalina">Catalina</a> and its primary purpose is to persist GValue's and GObject's in and out of storage. In the spirit of CouchDb, the default serialization format is Json. However, when I get around to adding a serialization interface this could be avoided if desired.

The current storage mechanism is BerkeleyDB, but I hope to add a few interesting alternatives in the near future.

The 4 methods if interest are:

<table>
  <tr>
    <td><b>catalina_db_get</b></td>
    <td>Retrieve a GValue from storage</td>
  </tr>
  <tr>
    <td><b>catalina_db_set</b></td>
    <td>Persist a GValue to storage</td>
  </tr>
  <tr>
    <td><b>catalina_db_get_gobject</b></td>
    <td>Retrieve a GObject from storage</td>
  </tr>
  <tr>
    <td><b>catalina_db_set_gobject</b></td>
    <td>Serialize a GObject to storage</td>
  </tr>
</table>

By now you have figured out that the storage is key/value based which makes it an ideal candidate for a BTree. Currently, that is all that is supported. I do not see why Hash and Queue from BerkeleyDB cannot be added.

<h2><b>CatalinaDb</b></h2>
<blockquote>
The CatalinaDb object is the primary API interface to developers. It manages storage, indexes, and the entry point to persisting data.
</blockquote>

<h2><b>CatalinaStorage</b></h2>
<blockquote>
CatalinaStorage is an abstract class for storage implementations. Currently, only CatalinaBdbStorage is implemented.
</blockquote>

<h2><b>CatalinaBdbStorage</b></h2>
<blockquote>
Storage implementation using BerkeleyDB. A new bdb file is created for each index in the CatalinaDb.
</blockquote>

<h2><b>CatalinaIndex</b></h2>
<blockquote>
CatalinaIndex is an abstraction for storage implementations to wrap their internal handles for indexes while interacting with CatalinaDb.
</blockquote>

<h2><b>CatalinaTransaction</b></h2>
<blockquote>
CatalinaTransaction is an abstraction for storage implementations to wrap their internal handles for transactions while interacting with CatalinaDb.
</blockquote>

<h2><b>CatalinaIndexIter</b></h2>
<blockquote>
This is not yet implemented, but its design goal is to allow the developer to iterate through keys in a Db efficiently. It will probably have a hook for filtering what keys match as well.
</blockquote>

<h3>Things partially implemented</h3>
<ul>
  <li>Transactions</li>
  <li>Python bindings</li>
</ul>

<h3>Things I'd like to implement</h3>
<ul>
  <li>CatalinaIndexIter to iterate key/values in an index. This is a high priority for myself</li>
  <li>.NET/Mono bindings</li>
  <li>A serious test suite for regression testing</li>
  <li>SQLite BTree storage (not actually using sql, just the btree.c)</li>
  <li>Cache tuning for BerkeleyDB</li>
  <li>Alternative serialization</li>
  <li>Thread safety</li>
</ul>

<h3>Catalina Details</h3>
<ul>
  <li><a href="http://git.dronelabs.com/catalina/">Catalina Git Branch</a></li>
</ul>
