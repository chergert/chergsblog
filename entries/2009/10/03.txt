Author: Christian Hergert <chris@dronelabs.com>
Date: Sun, 04 Oct 2009 04:30:16 -0000
Title: GDateTime

I got fed up with working around all the limitations with date and time in glib and C (time_t, struct timeval, struct tm, GTimeVal, GDate, etc) so I decided to write something new.

The result is GDateTime.  It handles dates and times from 1/1/1 to 12/31/9999 on 100-nanosecond intervals.  As requested by a few individuals, it is an opaque/boxed type.  String parsing is incomplete but the rest should be in good shape.

The glib branch is available <a href="http://git.dronelabs.com/glib/?h=datetime">here</a>.  Or for those with short attention spans, the header <a href="http://git.dronelabs.com/glib/tree/glib/gdatetime.h?h=datetime">gdatetime.h</a>.

You can even use GDateTimes as keys within a GHashTable using g_date_time_hash, g_date_time_equal.

''' lang=c
GHashTable *hash = g_hash_table_new_full (g_date_time_hash, g_date_time_equal,
                                          g_date_time_free, NULL);
'''

I'm interested in feedback and patches.
