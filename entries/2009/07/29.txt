Author: Christian Hergert <chris@dronelabs.com>
Date: Wed, 29 Jul 2009 12:29:34 -0000
Title: Fun Hacks

Put together a small patch for gtk+ today that runs modal windows on OS X as the "pop-over sheet" typically found with Cocoa applications.

Obligatory screenshot below. And <a href="http://bugzilla.gnome.org/attachment.cgi?id=139446&amp;action=view">patch</a>. And <a href="http://bugzilla.gnome.org/show_bug.cgi?id=589084">Bugzilla #589084</a>.

<a href="http://x.dronelabs.com/chris/blog/screenshots/gtk/modal-sheet.png"><img src="http://x.dronelabs.com/chris/blog/screenshots/gtk/modal-sheet.png" width="300" /></a>
