Author: Christian Hergert <chris@dronelabs.com>
Date: Tue, 17 Feb 2009 16:55:11 -0000
Title: Marina

When I read that the Liferea hackers were considering doing a rewrite, I jumped at the opportunity to throw together a prototype.  I had been dabbling with this in python for the last year or so anyway, solving interesting problems with storing lots of data while keeping memory usage light.

I also wanted a project to start using <a href="http://docs.dronelabs.com/gtask/">GTask</a> so that I could find more blemishes to cleanup the API (more on that later). Interestingly enough, it was the perfect use case for my <a href="http://audidude.com/blog/?p=52">BdbListStore</a> which I blogged about a month or so ago.

In the process, I threw together a quick List+Hash implementation for a LRU to reduce the deserialization overhead. My C# implementation for <a href="http://audidude.com/blog/?p=62">Adroit</a> had this and it helped significantly. You can find it <a href="http://git.dronelabs.com/git?p=users/chris/marina-plugins.git;a=blob;f=marina/g-lru-cache.h;h=ab9ec151c26ca9ede0dd39fd915ba82ee048fd90;hb=HEAD">here</a>. Look at the BdbListStore in that directory for how to use it.

So below is a screenshot of what marina looks like right now. It's not complete by any means, but its a start.  It has a rich plugin architecture based on that of GEdit. I hope to foster even a fraction of the success that GEdit's plugin community has had.

<a href="http://audidude.com/blog-content/marina/screenshot2.png"><img src="http://audidude.com/blog-content/marina/screenshot2-thumb.png" style="float: right; padding: 10px;" /></a>
There is a rudimentary google reader plugin that shows how to use the plugin API. Python will be supported in the near future as soon as I finish the python bindings. I intend to add support for Seed and Mono as well, but time is incredibly limited.

<a href="http://audidude.com/blog-content/marina/screenshot3.png"><img src="http://audidude.com/blog-content/marina/screenshot3-thumb.png" style="float: left; padding: 10px;" /></a>
You'll notice that it also supports color-schemes. I still need to find a way for plugins to extend the html properly here. However, when webkit gets DOM support, life will be much much easier. I noticed that webkit will block when you call load_html()/execute_script() on a dns query as a result from getaddrinfo(). If you have really shitty dns, this is a problem. I'd fix it, but I think libsoup integration will make it a non-issue.

<a href="http://audidude.com/blog-content/marina/screenshot4.png"><img src="http://audidude.com/blog-content/marina/screenshot4-thumb.png" style="float: right; padding: 10px;" /></a>
At this point, you may be screaming at your monitor in disbelief that we need yet another f*cking feed reader. However, I disagree. Whats available is poor on memory and has an antiquated style of information management that has proven to fail over and over.

It doesn't quite look it now, as I'm busy building the infrastructure for the application, but Marina will be different.  It will learn what you like through the use of a bayessian plugin. It will watch your firefox browsing patterns and automatically bring in feeds that are similar to news that you are interested in. It will provide a generic syndication parser to the desktop so applications like gnome-shell can just ask for information and get it.

I also can't stand the perusal pattern of current readers. I've noticed that when I catch up on blogs and news that it takes 100% of my attention. Situations like these are perfect for fullscreen. I very much imagine a flashy newspaper view with my unread articles categorized by content as apposed to source. Similar to the sections of a newspaper for sports, entertainment and science.

You could even tie in social aspects with integration to myspace and twitter, or sharing your feeds with your friends over jabber.

And most importantly, they can all be plugins :-)

You can follow my branch at git.dronelabs.com or fork me on github.

'''
git clone git://github.com/chergert/marina.git
'''

I've created a PPA with dependencies for Ubuntu Intrepid Ibex.

'''
deb http://ppa.launchpad.net/audidude/ubuntu intrepid main
deb-src http://ppa.launchpad.net/audidude/ubuntu intrepid main
'''

'''
apt-get install libgtask-dev libgtask-doc librss-glib-dev
librss-glib-doc libjson-glib-dev libxml2-dev libgnome-keyring-dev
libwebkit-dev gnome-common libgconf2-dev gtk-doc-tools
uuid-dev libsoup2.4-dev libdb4.7-dev
'''
