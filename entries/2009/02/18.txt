Author: Christian Hergert <chris@dronelabs.com>
Date: Wed, 18 Feb 2009 14:12:42 -0000
Title: Tuesday Mini Hack

I took a break from working on my slides today to do a mini hack that had been crossing my mind.  I rarely need devhelps full gui, i just want quick access.

<a href="http://audidude.com/blog-content/ddg/screenshot1.png"><img src="http://audidude.com/blog-content/ddg/screenshot1.png" style="width: 350px;" /></a>

Since I ported gnome-do's relevance to python last september and wrote the async io file walker, it was a breeze.

<a href="http://audidude.com/blog-content/ddg/screenshot2.png"><img src="http://audidude.com/blog-content/ddg/screenshot2.png" style="width: 350px;" /></a>

It's a bit slow on updating the treeview when it scores all the items. This could be fixed by rotating between two models and updating the second model in a background thread. but it works for now.

'''
git clone git://github.com/chergert/ddg.git
'''
