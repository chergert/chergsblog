Author: Christian Hergert <chris@dronelabs.com>
Date: Tue, 18 Aug 2009 06:43:37 -0000
Title: Visualizing gtk+ exposures postmortem

As of yesterday, I'm 25.  It was a fun day of hiking, drinks, video games, and district9.

<p>In <a href="http://git.dronelabs.com/gdkrecord">gdkrecord</a>, I added recording of GdkWindow sizes and exposure areas so that I can troubleshoot some pesky drawing problems with a few custom widgets.</p>

<img src="http://x.dronelabs.com/chris/blog/screenshots/gdkrecord-exposures.png" width="300" />

<p>And HTML5 Video below or at <a href="http://www.youtube.com/watch?v=XdOLjd52R4E">http://www.youtube.com/watch?v=XdOLjd52R4E</a>.</p>

<!-- video controls="true" src="http://x.dronelabs.com/chris/blog/screencasts/gdkrecord-exposures.ogv" width="300">
You can view this HTML5 video at <a href="http://www.youtube.com/watch?v=XdOLjd52R4E">http://www.youtube.com/watch?v=XdOLjd52R4E</a>.
</video -->

>>> Usage
'''
./gdkrecord-tool exposures myrecordfile.out
'''
