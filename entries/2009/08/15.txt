Author: Christian Hergert <chris@dronelabs.com>
Date: Sun, 16 Aug 2009 05:03:10 -0000
Title: Gdk Event Loop Profiling

I put together a gtk-module yesterday to record events from the main loop for post-analysis.  I'm sure this has been done before. You can find it at <a href="http://git.dronelabs.com/gdkrecord/">http://git.dronelabs.com/gdkrecord/</a>.

'''
GTK_MODULES=gdkrecord ./myprog
'''

By default the output file is pid-prgname.gdkrecord.  You can override this with

'''
GDKRECORD_FILENAME=myoutfile
'''

Inside of the tools directory is <a href="http://git.dronelabs.com/gdkrecord/plain/tools/gdkrecord-tool">gdkrecord-tool</a>.  It can dump the information in plain text, xml, json, and csv as well as generate graphs like the one below.

'''
./gdkrecord-tool graph myprog.gdkrecord
'''

Which will generate myprog.gdrecord.png.  It only adds information on a few event types right now, but I'll continue adding information extractors as I need them.

<a href="http://x.dronelabs.com/chris/dropbox/gdkrecord.png"><img src="http://x.dronelabs.com/chris/dropbox/gdkrecord.png" width="300" /></a>
