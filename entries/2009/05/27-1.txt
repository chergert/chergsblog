Author: Christian Hergert <chris@dronelabs.com>
Date: Wed, 27 May 2009 16:46:09 -0000
Title: Ethos - Simplifying Plugins for GNOME Applications

<img src="http://audidude.com/dropbox/ethos-1.png" style="float: right; padding: 10px; width: 200px;" />Plenty of desktop applications have extension frameworks so that users may extend functionality for their own purposes.  However, many seem to invent their own plug-in system or use a framework tied to the applications source code language.  For example, there is pkg_resources for python and Mono.Addins for mono.  Both are great tools when confined to their respective language.  Where they do not fit well is in applications that want to simultaneously enable various language communities such as JavaScript, Python, Ruby, .NET, C, and Vala.

I really liked the plug-in system from <a href="http://www.gnome.org/projects/gedit/">gedit</a>, but it is licensed under the GPL.  It would also require a lot of changes to be re-usable outside of gedit.  I should know, thats what I did in my syndication reader prototype, <a href="http://audidude.com/blog/?p=79">Marina</a>.  The framework is simplistic and has been incredibly successful at converting users into application extenders.  If GNOME desktop applications can provide a single framework for extending applications, the barrier to entry becomes much lower for "repeat extenders".  We instantly re-purpose those application scripters into new applications.

Therefore, I put together a library named <a href="http://git.dronelabs.com/ethos">ethos</a> which contains a system similar to gedit's but under an LGPL-2.1 license.  It also supports multiple plug-in languages including Python, JavaScript, Vala, and C.  Mono support is partially there as well.  I need to learn the ruby-gnome2 framework so I can add ruby support.

In addition, there is the ethos-ui library which provides a reusable Gtk widget for configuring plug-ins during runtime.  The lovely screenshot above demonstrates this widget.  In it, you will see plugins written in C, Python, JavaScript, and C# all living within a single process and virtual-machines co-operating in harmony.

'''
git clone git://git.dronelabs.com/ethos
'''
